#!/usr/bin/env bash
# VERSION 0.0.1
#########################################################################################
#  stats-fastq.sh                                                                       #
#  Compute reads and bases numbers for a list of fastq files.                           #
#########################################################################################

#########################################################################################
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, ##
## INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       ##
## PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  ##
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   ##
## OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      ##
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              ## 
#########################################################################################

# PROGRAM CONFIGURATION BEGIN
__version__='0.0.1'
__author__='Corentin Hochart'
__email__='corentin.hochart.pro@gmail.com'
__credits__=["Corentin Hochart"]
__license__='GPL3'
__maintainer__='Corentin Hochart'
__status__='Development'

export filename='random-fastq.sh'

# Declare functions
function error {
    error=$1
    if ! [ -z $2 ]; then 
        usage
        >&2 echo "error: $error"
    else 
        date=$(date "+%Y-%m-%d %H:%M:%S")
        >&2 echo "[$date] ERROR: $error. Execution halted."
        end=$(date +%s)
        warning "Runtime: $((end-start)) sec"
    fi
    exit 1
}

function warning {
    message=$@
    date=$(date "+%Y-%m-%d %H:%M:%S")
    >&2 echo "[$date] INFO: $message"
}

function usage {
    >&2 echo "usage: sbatch $filename [--help] [--in IN] [--out OUT]"
}

function help {
    >&2 echo "
                ...::: $filename v$__version__ :::...
            "
    usage
    >&2 echo "
Examples :
$filename.sh --in sample.tsv --out stats.tsv 

Synopsis :
Compute reads and bases numbers for a list of fastq files.

Arguments : 
    -h ,--help  Print this help and exit.
    --in        Input tabular samples file.
    --out       Output statistics files. 

Written by Corentin Hochart (corentin.hochart.pro@gmail.com).
Released under the terms of the GNU General Public License v3.
$filename version $__version__.
"
}

function fix_base_count() {
    local counts=($(cat))
    echo -e "${counts[0]}\t$((${counts[1]} - ${counts[0]}))"
}

# Greet the user
start=$(date +%s)
date=$(date)
warning "Date : $date"
date=$(date +%Y%m%d)
warning "Hi $USER. Lets do some good job together"

# Get options
arguments=$@
basename=$(echo ${0##*/})

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            help
            exit 0
            ;;
        --in)
            shift
                if test $# -gt 0; then
                    export input=$1
                else 
                    error 'argument --in: expected one argument' usage
                fi
            shift
            ;;
        --out)
            shift
                if test $# -gt 0; then
                    export output=$1
                else 
                    error 'argument --out: expected one argument' usage
                fi
            shift
            ;;
        *)
            error "Unknown option $1" 
            break
            ;;
    esac
done

# Check arguments
if [[ -z "$input" ]]; then
    error "the following arguments are required: --in" usage 
fi

if [[ -z "$output" ]]; then
    error "the following arguments are required: --out" usage 
fi

# Initialisation
warning "$filename v$__version__"
warning "$filename $arguments"

# Main code
echo -e "SAMPLE_ID\tFORWARD_READS\tFORWARD_BASES\tREVERSE_READS\tREVERSE_BASES\tUNPAIRED_READS\tFORWARD_READS" > $output
while IFS= read -r line
do

    IFS=$'\t' read -r -a line_array <<< "$line"
    if [ ${line_array[0]:0:1} == "#" ]; then
        continue 
    fi
    sample=${line_array[0]}
    warning $sample
    echo -ne $sample'\t'
    for i in $(seq 1 3)
    do
        if [ $i -eq 3 ]; then
            delimiter='\n'
        else 
            delimiter='\t'
        fi
        if [ ! -z ${line_array[$i]} ] ; then 
            IFS=',' read -r -a run <<< "${line_array[$i]}"
            echo -en $(for fastq in "${run[@]}"
            do
                if [ ${fastq: -3} == ".gz" ]; then
                    gzip -dc $fastq | awk 'NR % 4 == 2' | wc -cl | fix_base_count
                elif [ ${fastq: -3} == ".fq" ] || [ ${fastq: -6} == ".fastq" ] ; then
                    awk 'NR % 4 == 2' $fastq | wc -cl | fix_base_count
                fi
            done | awk 'BEGIN{FS=OFS=" "}
                {for (i=1;i<=NF;i++) a[i]+=$i}
                END{for (i=1;i<=NF;i++) printf a[i] OFS;}') | tr ' ' '\t' 
            echo -ne $delimiter
        else 
            echo -ne '\t'$delimiter
        fi 
    done
done < "$input" >> $output

# Exit code 
warning "$filename done"
end=$(date +%s)
warning "Runtime: $((end-start)) sec"

exit 0