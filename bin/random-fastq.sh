#!/usr/bin/env bash
# VERSION 0.1.0
#########################################################################################
#  random-fastq.sh                                                                      #
#  Randomly selected paired sequences or singleton in fastq file(s).                    #
#########################################################################################

#########################################################################################
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, ##
## INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       ##
## PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  ##
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   ##
## OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      ##
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              ## 
#########################################################################################

# PROGRAM CONFIGURATION BEGIN
__version__='0.1.0'
__author__='Corentin Hochart'
__email__='corentin.hochart.pro@gmail.com'
__credits__=["Corentin Hochart"]
__license__='GPL3'
__maintainer__='Corentin Hochart'
__status__='Development'
__lab__="Laboratoire d'Ecogéochimie des Environnements Benthiques"

export filename='random-fastq.sh'

# Declare functions
function error {
    error=$1
    if ! [ -z $2 ]; then 
        usage
        >&2 echo "error: $error"
    else 
        date=$(date "+%Y-%m-%d %H:%M:%S")
        >&2 echo "[$date] ERROR: $error. Execution halted."
        end=$(date +%s)
        warning "Runtime: $((end-start)) sec"
    fi
    exit 1
}

function warning {
    message=$@
    date=$(date "+%Y-%m-%d %H:%M:%S")
    >&2 echo "[$date] INFO: $message"
}

function usage {
    >&2 echo "usage: sbatch $filename [--help] [--in IN] [--in2 IN2] [--out OUT] [--out2 OUT2]
                              [--seed N] [--sub N]"
}

function help {
    >&2 echo "
                ...::: $filename v$__version__ :::...
            "
    usage
    >&2 echo "
Examples :
randomseq-fastq.sh --in file.fastq --out file.sff.fastq --sub 1000
randomseq-fastq.sh --in R1.fastq --in2 R2.fastq --out R1.sff.fastq --out2 R2.sff.fastq --sub 1000

Synopsis :
Randomly selected paired sequences or singleton in fastq file(s).

Arguments : 
    -h ,--help  Print this help and exit.
    --in        Primary reads input; required parameter.
                (fastq format)
    --in2       For paired reads in two files.
                If used '--in' file reads and '--in2' file reads 
                must be in the same order.
    --out       Write subsample reads; required parameter.
    --out2      Use this to write 2nd read of pairs.
    --program    command line program to use to shuffle reads
                {'shuf','sort'}.
    --seed      Fixed seed in order to reobtain the same random 
                reads. 
    --sub       Number of reads (or paired reads) to keep for
                the subsampling.

Written by Corentin Hochart* (corentin.hochart.pro@gmail.com).
Released under the terms of the GNU General Public License v3.
$filename version $__version__.
*Awk code based on LINDENBAUM Pierre comment (https://www.biostars.org/p/6544/)
    "
}

function get_seeded_random {
    seed="$1"
    openssl enc -aes-256-ctr -pass pass:"$seed" -nosalt </dev/zero 2>/dev/null
}

# Print wrapper usage if no arguments
NUMARGS=$#
if [ "$NUMARGS" -eq 0 ]; then
    usage
fi

# Greet the user
start=$(date +%s)
date=$(date)
warning "Date : $date"
date=$(date +%Y%m%d)
warning "Hi $USER. Lets do some good job together"

# Get options
arguments=$@
SUB=100
program='shuf'
basename=$(echo ${0##*/})
SEED=$((1 + RANDOM % 1000)) 

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            help
            exit 0
            ;;
        --program)
            shift
                if test $# -gt 0; then
                    export program=$1
                else 
                    error 'argument --program: expected one argument' usage
                fi
            shift
            ;;
        --in)
            shift
                if test $# -gt 0; then
                    export IN=$1
                else 
                    error 'argument --in: expected one argument' usage
                fi
            shift
            ;;
        --in2)
            shift
                if test $# -gt 0; then
                    export IN2=$1
                else 
                    error 'argument --in2: expected one argument' usage
                fi
            shift
            ;;
        --out)
            shift
                if test $# -gt 0; then
                    export OUT=$1
                else 
                    error 'argument --out: expected one argument' usage
                fi
            shift
            ;;
        --out2)
            shift
                if test $# -gt 0; then
                    export OUT2=$1
                else 
                    error 'argument --out2: expected one argument' usage
                fi
            shift
            ;;
        --seed)
            shift
                if test $# -gt 0; then
                    export SEED=$1
                else 
                    error 'argument --seed: expected one argument' usage
                fi
            shift
            ;;
        --sub)
            shift
                if test $# -gt 0; then
                    export SUB=$1
                else 
                    error 'argument --sub: expected one argument' usage
                fi
            shift
            ;;
        *)
            error "Unknown option $1" 
            break
            ;;
    esac
done

# Check arguments
if [[ -z "$IN" ]]; then
    error "the following arguments are required: --in" usage 
fi

if [[ -z "$OUT" ]]; then
    error "the following arguments are required: --out" usage 
fi

if [[ $program != 'shuf' ]] && [[ $program != 'sort' ]] ; then
    error "$filename: error: argument --program: invalid choice: $program (choose from shuf, sort)" usage 
fi

# Initialisation
warning "$filename v$__version__"
warning "$filename $arguments"
warning "seed number: $SEED"

# Main code
if [ ! -z $IN2 ];then
    if [ -z $OUT2 ];then
        usage
    fi
    if [[ $IN =~ gz$ ]]; then 
        IN="<(zcat $IN)"
    fi
    if [[ $IN2 =~ gz$ ]]; then 
        IN2="<(zcat $IN2)"
    fi
    warning "Subsampling $SUB paired sequences"
    eval paste $IN $IN2 | awk '{ printf("%s",$0); n++; if(n%4==0) { printf("\n");} else { printf("\t\t");} }' |\
    $program --random-source=<(get_seeded_random $SEED) | head -$SUB | sed 's/\t\t/\n/g' | awk -F'\t' '{print $1 > "'$OUT'"; print $2 > "'$OUT2'"}'
else
    warning "Subsampling $SUB sequences"
    if [[ $IN =~ gz$ ]]; then 
        awk '{ printf("%s",$0); n++; if(n%4==0) { printf("\n");} else { printf("\t\t");} }' <(zcat $IN) |\
        $program --random-source=<(get_seeded_random $SEED) | head -$SUB | sed 's/\t\t/\n/g' > $OUT
    else 
        awk '{ printf("%s",$0); n++; if(n%4==0) { printf("\n");} else { printf("\t\t");} }' $IN |\
        $program --random-source=<(get_seeded_random $SEED) | head -$SUB | sed 's/\t\t/\n/g' > $OUT
    fi
fi

# Exit code 
warning "$filename done"
end=$(date +%s)
warning "Runtime: $((end-start)) sec"

exit 0